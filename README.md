# AeroTerm
Built in Qt5 (and will likely work with Qt6), AeroTerm is a Windows/Linux/macOS terminal for aeroboticists and other makers who are constantly plugging into serial ports. One of the biggest benefit of this terminal is that it updates serial port settings instantly, without requiring a close/open cycle. This takes a lot of the clicking out of trying to guess what baud rate and port settings a hardware device requires.

## Releases
**Releases**: https://gitlab.com/kubark42/AeroTerm/-/releases

## Features

- Simple and intuitive to use
- Hex view and plain text view
- Visualizes when in the data stream the user sent data to the device
- Search field for making patterns "pop"
- Can be configured to send keyboard input immediately or in batch
- Record and playback logfiles
  - Saves the logfile in hexadecimal escape encoding in a JSON file.
  - Playback speed can be adjusted. This is very helpful for reproducing embedded hardware errors..
- Change serial port settings without forcing the user to close and re-open the port. Saves clicks!
- Can send raw hex by using hexadecimal escapes, e.g. `\x00\x30\x31\xff` is the equivalent of `0x003031FF`


## Screenshots

### Plain text view
![AeroTerm_screenshot__plain_](https://gitlab.com/kubark42/AeroTerm/uploads/ea242df8f18c45a437a87a272db8c4de/Screen_Shot_2021-08-03_at_14.01.18.png)

### Hex view
![AeroTerm_screenshot__hex_](https://gitlab.com/kubark42/AeroTerm/uploads/99056313eb0cfad6af9f758d455b3d08/Screen_Shot_2021-08-03_at_14.01.20.png)

### Hexadecimal escape encoding
![Hexadecimal](https://gitlab.com/kubark42/AeroTerm/uploads/717370e35979700c273003bb846a6c2d/Screen_Shot_2021-08-03_at_14.01.42.png)

### Tick marks for visualizing when keyboard data was sent
![AeroTerm_screenshot_tickmarks](/uploads/3f7eafaeeb0cab81218799d0ba20d731/AeroTerm_screenshot_tickmarks.png)

## Known issues

- Ugly as sin
  - I would love to fix this, but I'll need user feedback.
- As implemented, the TextEdit view seems to have performance issues as the scrollback queue grows. This is most obvious when playing back a dense logfile at 100x.
- Recordes samples at a hardcoded 10Hz, which is not fast enough for some purposes

## Updates and contributions
If there haven't been any updates in a while, it's because this is feature complete for every feature I've been asked to complete. If you like the idea, but feel a feature is missing, please don't hesitate to start an issue for it!

## Building
Bog standard Qt build. Load `AeroTerm.pro` into Qt Creator and hit the big build button. This should always work, if the build fails, let me know and I'll fix it.
