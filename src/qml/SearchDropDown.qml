import QtQuick

import QtQuick.Controls
import QtQuick.Layouts

Rectangle {
    id: self
    property alias searchListView: searchListView

    visible: (opacity == 0.0) ? false : true

    radius: 4
    color: "lightgray"

    border.color: "gray"
    border.width: 0.5

    width: 140
    height: searchListView.listHeight + 240  // Seems to be a good round number

    clip: true

    property int buttonSelection: 0

    ButtonGroup {
        id: radioGroup
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.topMargin: 4
        anchors.bottomMargin: 4
        anchors.leftMargin: 4
        spacing: 0

        RadioButton {
            ButtonGroup.group: radioGroup

            checked: true

            text: qsTr("Text search")

            onCheckedChanged: {
                console.log("[Search] string search activated")
                self.opacity = 0

                buttonSelection = 0
            }
        }
        RadioButton {
            ButtonGroup.group: radioGroup

            text: qsTr("Literal search")

            onCheckedChanged: {
                console.log("[Search] literal search activated")
                self.opacity = 0

                buttonSelection = 1
            }
        }
        RadioButton {
            ButtonGroup.group: radioGroup

            text: qsTr("Hex search")

            onCheckedChanged: {
                console.log("[Search] hex search activated")
                self.opacity = 0

                buttonSelection = 2
            }
        }

        Item {
            Layout.preferredHeight: 16
            Text {
                text: "----------------------------"
                color: "gray"
            }
        }
        Item {
            Layout.preferredHeight: 16
            Text {
                text: "Recent searches"
                color: "gray"
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout {
                id: vert_layout
                spacing: 0

                ListView {
                    id: searchListView
                    readonly property int itemPadding: 4
                    readonly property int itemHeight: tm.height + itemPadding * 2
                    readonly property TextMetrics tm: TextMetrics { text: "#" }
                    property int visibleItems: model.count
                    property real listHeight: visibleItems * itemHeight
                    Layout.preferredHeight: listHeight
                    Layout.minimumWidth: contentItem.childrenRect.width + itemPadding * 2
                    Layout.fillWidth: true

                    clip: true
                    model:  ListModel {
                    }

                    delegate: Item {
                        width: itemLabel.width
                        height: searchListView.itemHeight

                        Item {
                            width: searchListView.width
                            height: parent.height
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    console.log("Clicked!" + index)
                                    searchListView.currentIndex = index
                                }
                            }
                            Label {
                                id: itemLabel
                                anchors.centerIn: parent
                                text: searchString
                            }
                        }
                    }
                }
            }
        }

        Item {
            visible: searchListView.model.count > 0
            Layout.preferredHeight: 16
            Text {
                text: "----------------------------"
                color: "gray"
            }
        }
        Item {
            visible: searchListView.model.count > 0
            Layout.preferredHeight: 16
            Layout.fillWidth: true
            Text {
                text: "Clear"
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("Clicked!")
                    searchListView.model.clear()
                }
            }
        }

    }
}
