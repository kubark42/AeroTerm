import QtQuick 2.15

Item {
    width: 100
    height: 40

    Rectangle {
        id: rect_connectionFlag

        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height

        radius: 10

        Text {
            id: flagText

            anchors.fill: parent
            anchors.centerIn: parent
            color: "white"

            horizontalAlignment : Text.AlignHCenter
            verticalAlignment : Text.AlignVCenter
            topPadding: 10
        }

        Behavior on y {
            SmoothedAnimation { velocity: 150 }
        }

        Behavior on color {
            ColorAnimation {}
        }
    }

    onStateChanged: {
        if (state === "fsm_reconnected") {
            autoHide.restart()
        } else {
            autoHide.stop()
        }
    }

    states: [
        State {
            name: "fsm_disconnected"
            PropertyChanges { target: rect_connectionFlag; color: "red" }
            PropertyChanges { target: rect_connectionFlag; y: -10 }
            PropertyChanges { target: flagText; text: "Disconnected" }
        },
        State {
            name: "fsm_reconnected"
            PropertyChanges { target: rect_connectionFlag; color: "green" }
            PropertyChanges { target: flagText; text: "Reconnected" }
            PropertyChanges { target: rect_connectionFlag; y: -10 }
        }
    ]

    Timer {
        id: autoHide
        interval: 750
        repeat: false

        onTriggered: {
            rect_connectionFlag.y = -rect_connectionFlag.height
        }
    }
}
