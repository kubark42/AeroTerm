#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>

#include "cpp/third_party/ql-serial/src/lib/ql-channel-serial.hpp"
#include "cpp/third_party/QtSyntaxHighlighter/SyntaxHighlighter.h"
#include "cpp/third_party/QtSyntaxHighlighter/TextCharFormat.h"
#include "cpp/fileio.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    // Set QML style. Could be Material, Universal, Basic, etc... Doing it
    // here sets it app-wide.
    QQuickStyle::setStyle("Basic");

    app.setOrganizationName("CyPi Ltd");
    app.setOrganizationDomain("cypiltd.com");
    app.setApplicationName("AeroTerm");

    qmlRegisterType<QlChannelSerial>("QlChannelSerial", 1,0, "QlChannelSerial");
    qmlRegisterType<FileIO>("FileIO", 1,0, "FileIO");
    qmlRegisterType<SyntaxHighlighter>("SyntaxHighlighter", 1,0, "SyntaxHighlighter");
    qmlRegisterType<TextCharFormat>("SyntaxHighlighter", 1,0, "TextCharFormat");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/qml/main.qml")));

    return app.exec();
}
